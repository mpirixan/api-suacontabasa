#!/bin/bash

cd /home/springapi/api-suacontabasa

echo "Atualiza código fonte"
git pull remote origin master

echo "Build "

bash mvn clean package

echo "Docker build"
docker build -t springio/api-suacontabasa

echo "Docker RUN"
docker run -p 8080:8080 springio/api-suacontabasa
...