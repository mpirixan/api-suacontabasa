package br.com.bancoamazonia.api.suacontabasa.domain.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum TipoEmpresaEnum implements IEnumObject<String,String> {

	LTDA("Sociedade Limitada"),
	SA("Sociedade Anônima"),
	MEI("Microempreendedor Individual"),
	EI("Empresário Individual"),
	SLU("Sociedade Limitada Unipessoal");
	
	private String value;
	
	public String getValue() {
		return this.value;
		
	}
	
	private TipoEmpresaEnum(String value) {
		this.value = value;
		
	}
	
	@Override
	public String getKey() {
		return this.name();
	}
}
