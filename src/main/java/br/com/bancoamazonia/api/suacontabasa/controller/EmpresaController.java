package br.com.bancoamazonia.api.suacontabasa.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.bancoamazonia.api.suacontabasa.controller.dto.CadastroEmpresaRequest;
import br.com.bancoamazonia.api.suacontabasa.controller.dto.EmpresaResponse;
import br.com.bancoamazonia.api.suacontabasa.manager.EmpresaManager;
import io.swagger.annotations.Api;

@CrossOrigin(origins = "http://127.0.0.1:3000")
@RestController
@Api
@RequestMapping(value = "/empresa")
public class EmpresaController {

	@Autowired
	private EmpresaManager manager;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@GetMapping(value = "/idempresa/{idEmpresa}")
	public EmpresaResponse findById(@PathVariable("idEmpresa") Long idEmpresa){
		return modelMapper.map(manager.findByIdEmpresa(idEmpresa),EmpresaResponse.class);
	}
	
	@GetMapping(value = "/nomefantasia/{nomeFantasia}")
	public EmpresaResponse findByNomeFantasia(@PathVariable("nomeFantasia") String nomeFantasia) {
		return modelMapper.map(manager.findByNomeFantasia(nomeFantasia), EmpresaResponse.class);
	}
	
	
	@PostMapping(value="/cadastro")
	public CadastroEmpresaRequest insert(@RequestBody CadastroEmpresaRequest obj) {
		return modelMapper.map(manager.insert(obj), CadastroEmpresaRequest.class);
	}
	
	
	
	@DeleteMapping(value="/desativacao/{idEmpresa}")
	public void delete(@PathVariable("idEmpresa") Long idEmpresa) {
		 manager.delete(idEmpresa);
	}
}
