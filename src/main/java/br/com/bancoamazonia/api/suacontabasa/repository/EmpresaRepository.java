package br.com.bancoamazonia.api.suacontabasa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.bancoamazonia.api.suacontabasa.domain.model.Empresa;

public interface EmpresaRepository extends JpaRepository<Empresa, Long>{
	
	public Empresa findByIdEmpresa(Long idEmpresa);
	
	public Empresa findByIdFiscal(Long idFiscal);
	
	public Empresa findByNomeFantasia(String nomeFantasia);
}
