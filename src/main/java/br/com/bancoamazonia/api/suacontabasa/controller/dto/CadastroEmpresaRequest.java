package br.com.bancoamazonia.api.suacontabasa.controller.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import br.com.bancoamazonia.api.suacontabasa.domain.enums.TipoEmpresaEnum;
import io.swagger.v3.oas.annotations.media.Schema;

public class CadastroEmpresaRequest implements Serializable {

	private static final long serialVersionUID = 843512186486797943L;
	
	@Schema(description = "IdFiscal")
	private Long idFiscal;
	
	@Schema(description = "Nome Fantasia")
	private String nomeFantasia;
	
	@Schema(description = "DAta de Criação")
	private LocalDate dataCriacao;
	
	@Schema(description = "Telefone")
	private Long telefone;
	
	@Schema(description = "Tipode de Empresa")
	private TipoEmpresaEnum tipoEmpresa;
	
	

	@Override
	public int hashCode() {
		return Objects.hash(dataCriacao, idFiscal, nomeFantasia, telefone, tipoEmpresa);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CadastroEmpresaRequest other = (CadastroEmpresaRequest) obj;
		return Objects.equals(dataCriacao, other.dataCriacao) && Objects.equals(idFiscal, other.idFiscal)
				&& Objects.equals(nomeFantasia, other.nomeFantasia) && Objects.equals(telefone, other.telefone)
				&& Objects.equals(tipoEmpresa, other.tipoEmpresa);
	}

	public Long getIdFiscal() {
		return idFiscal;
	}

	public void setIdFiscal(Long idFiscal) {
		this.idFiscal = idFiscal;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public LocalDate getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(LocalDate dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Long getTelefone() {
		return telefone;
	}

	public void setTelefone(Long telefone) {
		this.telefone = telefone;
	}

	public TipoEmpresaEnum getTipoEmpresa() {
		return tipoEmpresa;
	}

	public void setTipoEmpresa(TipoEmpresaEnum tipoEmpresa) {
		this.tipoEmpresa = tipoEmpresa;
	}
	
	
}
