package br.com.bancoamazonia.api.suacontabasa.controller.dto;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Objects;

import io.swagger.v3.oas.annotations.media.Schema;

public class EmpresaResponse implements Serializable{
	
	private static final long serialVersionUID = 2876421466563848712L;

	@Schema(description = "idEmpresa")
	private Long idEmpresa;
	
	@Schema(description = "idFiscal ")
	private Long idFiscal;
	
	@Schema(description = "nome  fantasia")
	private String nomeFantasia;

	
	@Schema(description = "data criacao")
	private Calendar dataDeCriacao;
	
	@Schema(description="telefone")
	private Long telefone;	
	
	@Schema(description="tipo empresa")
	private String tipoEmpresa;
	
	public EmpresaResponse() {
		
	}

	@Override
	public int hashCode() {
		return Objects.hash(dataDeCriacao, idEmpresa, idFiscal, nomeFantasia, telefone, tipoEmpresa);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmpresaResponse other = (EmpresaResponse) obj;
		return Objects.equals(dataDeCriacao, other.dataDeCriacao) && Objects.equals(idEmpresa, other.idEmpresa)
				&& Objects.equals(idFiscal, other.idFiscal) && Objects.equals(nomeFantasia, other.nomeFantasia)
				&& Objects.equals(telefone, other.telefone) && Objects.equals(tipoEmpresa, other.tipoEmpresa);
	}

	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public Long getIdFiscal() {
		return idFiscal;
	}

	public void setIdFiscal(Long idFiscal) {
		this.idFiscal = idFiscal;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public Calendar getDataDeCriacao() {
		return dataDeCriacao;
	}

	public void setDataDeCriacao(Calendar dataDeCriacao) {
		this.dataDeCriacao = dataDeCriacao;
	}

	public Long getTelefone() {
		return telefone;
	}

	public void setTelefone(Long telefone) {
		this.telefone = telefone;
	}

	public String getTipoEmpresa() {
		return tipoEmpresa;
	}

	public void setTipoEmpresa(String tipoEmpresa) {
		this.tipoEmpresa = tipoEmpresa;
	}
	
	
}
