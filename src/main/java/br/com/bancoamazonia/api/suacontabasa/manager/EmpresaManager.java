package br.com.bancoamazonia.api.suacontabasa.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import br.com.bancoamazonia.api.suacontabasa.controller.dto.CadastroEmpresaRequest;
import br.com.bancoamazonia.api.suacontabasa.domain.model.Empresa;
import br.com.bancoamazonia.api.suacontabasa.manager.exceptions.DatabaseException;
import br.com.bancoamazonia.api.suacontabasa.manager.exceptions.ResourceNotFoundException;
import br.com.bancoamazonia.api.suacontabasa.repository.EmpresaRepository;

@Service
@Validated
public class EmpresaManager {

	
	@Autowired
	private EmpresaRepository repository;
	
	
	public Empresa findByIdEmpresa(Long idEmpresa) { 
	Empresa empresa = repository.findByIdEmpresa(idEmpresa);
	if (empresa == null) {
		throw new ResourceNotFoundException("Elemento não encontrado. IdEmpresa" + idEmpresa);

	}
		
		return empresa;
	}
	
	public Empresa findByNomeFantasia (String nomeFantasia){
		Empresa empresa = repository.findByNomeFantasia(nomeFantasia);
		if(empresa == null) {
			throw new ResourceNotFoundException("Elemento não encontrado: " + nomeFantasia);
		}
		return repository.findByNomeFantasia(nomeFantasia);
	}
	
	
	@Transactional
	public Empresa insert(CadastroEmpresaRequest obj) {
		Empresa empresa = new Empresa();
		Empresa entity = new Empresa();
		entity = repository.findByIdFiscal(obj.getIdFiscal());
		if (entity != null ) {
			throw new DatabaseException("Elemento já possui cadastro no sistema. CNPJ: " + obj.getIdFiscal());
		}
		empresa.setIdFiscal(obj.getIdFiscal());
		empresa.setConta(null);
		empresa.setNomeFantasia(obj.getNomeFantasia());
		empresa.setDataDeCriacao(obj.getDataCriacao());
		empresa.setTelefone(obj.getTelefone());
		empresa.setTipoEmpresa(obj.getTipoEmpresa());	
		return repository.save(empresa);
	}
	
	@Transactional
	public Empresa delete(Long idEmpresa) {
		Empresa entity = repository.findByIdEmpresa(idEmpresa);
		if(entity == null) {
			throw new ResourceNotFoundException("Elemento não encontrado. IDEMPRESA " + idEmpresa);
		}
		repository.delete(entity);
		return null;
	
}
	
}
