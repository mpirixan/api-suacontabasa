package br.com.bancoamazonia.api.suacontabasa.controller.dto;

import java.io.Serializable;

import br.com.bancoamazonia.api.suacontabasa.domain.enums.TipoContaEnum;
import io.swagger.v3.oas.annotations.media.Schema;

public class CadastroContaRequest implements Serializable{

	private static final long serialVersionUID = 1399392304940041785L;
	
	@Schema(description = "idAgencia - identificador agencia da Conta")
	private Long agencia;
	
	@Schema(description = "Tipo de Conta")
	private TipoContaEnum tipoConta;





	public Long getIdAgencia() {
		return agencia;
	}

	public void setIdAgencia(Long agencia) {
		this.agencia = agencia;
	}


	public TipoContaEnum getTipoConta() {
		return tipoConta;
	}

	public void setTipoConta(TipoContaEnum tipoConta) {
		this.tipoConta = tipoConta;
	}
	
	
}
