FROM openjdk:8-jdk-alpine
COPY target/api-suacontabasa.jar api-suacontabasa.jar
ENTRYPOINT ["java","-jar","/api-suacontabasa.jar"]